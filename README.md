# README #

This is supplemental material for a manuscript under development that is in the process of submission/consideration for publication. 

Any contents should be considered preliminary until this material has been accepted for publication. Please see the README file within the folder for additional information. 

The potential citation for this work will be: Dawson, D., Fisher, H., Noble, A.E., Meng, Q., Doherty, Sakano, Y, A.C., Sakano, Y., Vallero, D., Tornero-Velez, R., and Cohen-Hubal, E. 2022. An assessment of non-occupational 1,4-dioxane exposure pathways from drinking water and product use XXXXXX

The included work reflects the views of the authors, and does not necessarily represent the policy of the US Environmental Protection Agency. 